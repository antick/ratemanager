using System.Reflection;
using System.Runtime.CompilerServices;


[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.RateManager")]
[assembly: AssemblyCopyright("Copyright � Antick 2017")]
[assembly: AssemblyTrademark("Antick.RateManager")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]