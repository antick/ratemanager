﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Antick.RateManager.Client")]
[assembly: AssemblyDescription("Antick.RateManager api client")]
[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.RateManager.Client")]
[assembly: AssemblyCopyright("Copyright © Antick 2017")]
[assembly: AssemblyTrademark("Antick.RateManager.Client")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: Guid("636ba43c-8ada-4e8c-a125-5ea3c44743b2")]