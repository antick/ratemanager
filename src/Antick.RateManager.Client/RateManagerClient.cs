﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using RestSharp;

namespace Antick.RateManager.Client
{
    public class RateManagerClient: IRateManagerClient
    {
        private readonly string m_Address;

        public RateManagerClient(string address)
        {
            m_Address = address;
        }

        public async Task<List<Rate>> GetRates(string instrument, TimeFrameType type, string value, DateTime startTime, DateTime endTime, bool onlyCompleted = false)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/rates", Method.POST);
            request.AddJsonBody(new
            {
                Instrument = instrument,
                TimeFrameType = type,
                TimeFrameValue = value,
                StartTime = startTime,
                EndTime = endTime,
                OnlyCompleted = onlyCompleted
            });

            var resp = client.Execute<List<Rate>>(request);
            return resp.Data;
        }

        public async Task<List<Rate>> GetRatesByIndexes(string instrument, TimeFrameType type, string value, long startIndex, long endIndex)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/rates/byIndexes", Method.POST);
            request.AddJsonBody(new
            {
                Instrument = instrument,
                TimeFrameType = type,
                TimeFrameValue = value,
                StartIndex = startIndex,
                EndIndex = endIndex
            });

            var resp = client.Execute<List<Rate>>(request);
            return resp.Data;
        }

        public async Task<List<Rate>> GetRatesLast(string instrument, TimeFrameType type, string value, int count)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/rates/last", Method.POST);
            request.AddJsonBody(new
            {
                Instrument = instrument,
                TimeFrameType = type,
                TimeFrameValue = value,
                Count = count
            });

            var resp = client.Execute<List<Rate>>(request);
            return resp.Data;
        }

        public async Task<List<Rate>> GetRatesByLastTime(string instrument, TimeFrameType type, string value, DateTime endTime, int count)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/rates/byLastTime", Method.POST);
            request.AddJsonBody(new
            {
                Instrument = instrument,
                TimeFrameType = type,
                TimeFrameValue = value,
                Count = count,
                EndTime = endTime
            });

            var resp = client.Execute<List<Rate>>(request);
            return resp.Data;
        }

        public async Task<Rate> GetFirstRate(string instrument, TimeFrameType type, string value)
        {
            var client = new RestClient(m_Address);
            var request = new RestRequest("api/rates/getFirst", Method.POST);
            request.AddJsonBody(new
            {
                Instrument = instrument,
                TimeFrameType = type,
                TimeFrameValue = value
            });

            var resp = client.Execute<Rate>(request);
            return resp.Data;
        }
    }
}
