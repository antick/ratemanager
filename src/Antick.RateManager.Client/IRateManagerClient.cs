﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;

namespace Antick.RateManager.Client
{
    public interface IRateManagerClient
    {
        /// <summary>
        /// Получение данных в рамках указанной границы времени
        /// </summary>
        Task<List<Rate>> GetRates(string instrument, TimeFrameType type, string value, DateTime startTime, DateTime endTime, bool onlyCompleted = false);

        Task<List<Rate>> GetRatesByIndexes(string instrument, TimeFrameType type, string value, long startIndex, long endIndex);

        Task<List<Rate>> GetRatesLast(string instrument, TimeFrameType type, string value, int count);

        /// <summary>
        /// Получение данных строго до указанного времени, и указанного кол-ва баров
        /// </summary
        Task<List<Rate>> GetRatesByLastTime(string instrument, TimeFrameType type, string value, DateTime endTime,
            int count);

        /// <summary>
        /// Получения первой котировки
        /// </summary>
        Task<Rate> GetFirstRate(string instrument, TimeFrameType type, string value);
    }
}
