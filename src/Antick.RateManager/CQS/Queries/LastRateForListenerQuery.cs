﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Queries
{
    public class LastRateForListenerQuery : IQuery<GetLastRateForListener, RateModel>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;

        public LastRateForListenerQuery(ISessionBuilder<EntityContext> session)
        {
            m_Session = session;
        }

        public RateModel Ask(GetLastRateForListener criterion)
        {
            return m_Session.Execute(session =>
            {
                RateModel result = null;

                var dbRate = session.Query<RateDto>()
                    .FirstOrDefault(p => p.ListenerId == criterion.ListenerId && p.Index == criterion.LastIndex);

                if (dbRate != null)
                    result = dbRate.ToModel();

                return result;
            });
        }
    }
}
