﻿using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Queries
{
    public class GetActiveListenersQuery : IQuery<GetActiveListeners, List<ListenerModel>>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;
        private readonly IQueryBuilder m_QueryBuilder;

        public GetActiveListenersQuery(ISessionBuilder<EntityContext> session, IQueryBuilder queryBuilder)
        {
            m_Session = session;
            m_QueryBuilder = queryBuilder;
        }

        public List<ListenerModel> Ask(GetActiveListeners criterion)
        {
            var listeners =  m_Session.Execute(session =>
            {
                // Db запрос с материализацией
                var dbListeners = session.Query<ListenerDto>().Where(p => p.Enabled).ToList();

                // Памминг
                List<ListenerModel> result = new List<ListenerModel>();
                result.AddRange(dbListeners.Select(x => x.ToModel()));
                return result;
            });

            // Установого последнего Rate для каждого Listener
            foreach (var listener in listeners)
            {
                if (listener.LastRateIndex != null)
                {

                    var lastRate = m_QueryBuilder.For<RateModel>()
                        .With(new GetLastRateForListener {ListenerId = listener.Id, LastIndex = listener.LastRateIndex.Value });

                    listener.UpdateLastRate(lastRate);
                }
            }

            return listeners;
        }
    }
}
