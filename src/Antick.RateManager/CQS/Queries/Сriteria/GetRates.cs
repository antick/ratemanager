﻿using System;
using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;

namespace Antick.RateManager.CQS.Queries.Сriteria
{
    public class GetRates : ICriterion
    {
        public string Instrument { get; set; }

        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        /// <summary>
        /// Когда true - выдает только завершенные котировки( исключает самую последнюю )
        /// </summary>
        public bool OnlyCompleted { get; set; }

    }
}
