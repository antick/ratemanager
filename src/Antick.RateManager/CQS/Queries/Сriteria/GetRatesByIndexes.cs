﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;

namespace Antick.RateManager.CQS.Queries.Сriteria
{
    public class GetRatesByIndexes : ICriterion
    {
        public string Instrument { get; set; }

        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public long StartIndex { get; set; }

        public long EndIndex { get; set; }
    }
}
