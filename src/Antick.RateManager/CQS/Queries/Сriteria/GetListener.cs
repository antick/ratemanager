﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;

namespace Antick.RateManager.CQS.Queries.Сriteria
{
    public class GetListener : ICriterion
    {
        public string Instrument { get; set; }
        public TimeFrameComplex TimeFrame { get; set; }
    }
}
