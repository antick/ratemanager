﻿using System;
using Antick.Contracts.Domain;
using Antick.Cqrs.Queries;

namespace Antick.RateManager.CQS.Queries.Сriteria
{
    public class GetRatesByLastTime : ICriterion
    {
        public string Instrument { get; set; }

        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public int Count { get; set; }

        public DateTime EndTime { get; set; }
    }
}
