﻿using Antick.Cqrs.Queries;

namespace Antick.RateManager.CQS.Queries.Сriteria
{
    public class GetLastRateForListener : ICriterion
    {
        public int ListenerId { get; set; }

        public long LastIndex { get; set; }
    }
}
