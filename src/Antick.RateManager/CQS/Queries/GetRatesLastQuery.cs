﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;

namespace Antick.RateManager.CQS.Queries
{
    public class GetRatesLastQuery : IQuery<GetRatesLast, List<Rate>>
    {
        private readonly ISessionBuilder<EntityContext> m_SessionBuilder;
        private readonly IListenerCache m_ListenerCache;

        public GetRatesLastQuery(ISessionBuilder<EntityContext> sessionBuilder, IListenerCache listenerCache)
        {
            m_SessionBuilder = sessionBuilder;
            m_ListenerCache = listenerCache;
        }

        public List<Rate> Ask(GetRatesLast criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var listener = m_ListenerCache.GetByTimeFrame(criterion.Instrument, criterion.TimeFrameType,
                    criterion.TimeFrameValue);

                if (listener == null)
                    return new List<Rate>();

                var ratesDtos = session.Query<RateDto>()
                    .Where(p => p.ListenerId == listener.Id)
                    .OrderByDescending(p => p.Index)
                    .Take(criterion.Count)
                    .ToList()
                    .OrderBy(p => p.Index)
                    .ToList();
                
                return ratesDtos.Select(p => p.ToDomain()).ToList();
            });
        }
    }
}
