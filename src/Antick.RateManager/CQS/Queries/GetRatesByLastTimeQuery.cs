﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Queries
{
    public class GetRatesByLastTimeQuery : IQuery<GetRatesByLastTime, List<Rate>>
    {
        private readonly ISessionBuilder<EntityContext> m_SessionBuilder;
        private readonly IListenerCache m_ListenerCache;

        public GetRatesByLastTimeQuery(ISessionBuilder<EntityContext> sessionBuilder, IListenerCache listenerCache)
        {
            m_SessionBuilder = sessionBuilder;
            m_ListenerCache = listenerCache;
        }

        public List<Rate> Ask(GetRatesByLastTime criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var listener = m_ListenerCache.GetByTimeFrame(criterion.Instrument, criterion.TimeFrameType,
                    criterion.TimeFrameValue);

                if (listener == null)
                    return new List<Rate>(); ;

                var time = criterion.EndTime.ToUniversalTime();

                var ratesDtos = session.Query<RateDto>()
                    .Where(p => p.ListenerId == listener.Id && p.Time < time).OrderByDescending(p => p.Index)
                    .Take(criterion.Count).ToList();

                return ratesDtos.OrderBy(p => p.Index).Select(p => p.ToDomain()).ToList();
            });
        }
    }
}
