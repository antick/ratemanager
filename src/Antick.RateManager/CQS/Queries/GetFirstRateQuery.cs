﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Castle.Core.Logging;

namespace Antick.RateManager.CQS.Queries
{
    public class GetFirstRateQuery : IQuery<GetFirstRate, Rate>
    {
        private readonly ILogger m_Logger;
        private readonly ISessionBuilder<EntityContext> m_SessionBuilder;
        private readonly IListenerCache m_ListenerCache;

        public GetFirstRateQuery(ILogger logger, ISessionBuilder<EntityContext> sessionBuilder, IListenerCache listenerCache)
        {
            m_Logger = logger;
            m_SessionBuilder = sessionBuilder;
            m_ListenerCache = listenerCache;
        }

        public Rate Ask(GetFirstRate criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var listenerDto = m_ListenerCache.GetByTimeFrame(criterion.Instrument, criterion.TimeFrameType,
                    criterion.TimeFrameValue);

                if (listenerDto == null)
                    return null;

                var ratesDtos = session.Query<RateDto>()
                    .Where(p => p.ListenerId == listenerDto.Id).OrderBy(p => p.Index).FirstOrDefault();

                return ratesDtos.ToDomain();
            });
        }
    }
}
