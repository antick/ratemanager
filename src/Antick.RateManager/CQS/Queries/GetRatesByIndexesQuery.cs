﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;

namespace Antick.RateManager.CQS.Queries
{
    public class GetRatesByIndexesQuery : IQuery<GetRatesByIndexes,List<Rate>>
    {
        private ISessionBuilder<EntityContext> m_Session;
        private IQueryBuilder m_QueryBuilder;
        private readonly IListenerCache m_ListenerCache;

        public GetRatesByIndexesQuery(ISessionBuilder<EntityContext> session, IQueryBuilder queryBuilder, IListenerCache listenerCache)
        {
            m_Session = session;
            m_QueryBuilder = queryBuilder;
            m_ListenerCache = listenerCache;
        }

        public List<Rate> Ask(GetRatesByIndexes criterion)
        {
            return m_Session.Execute(session =>
            {
                var listener = m_ListenerCache.GetByTimeFrame(criterion.Instrument, criterion.TimeFrameType,
                    criterion.TimeFrameValue);

                if (listener == null)
                    return new List<Rate>();

                var ratesDtos = session.Query<RateDto>()
                    .Where(p => p.ListenerId == listener.Id && p.Index >= criterion.StartIndex &&
                                p.Index <= criterion.EndIndex).OrderBy(p => p.Index).ToList();
                
                return ratesDtos.Select(p => p.ToDomain()).ToList();
            });
        }
    }
}
