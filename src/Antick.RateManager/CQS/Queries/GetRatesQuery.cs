﻿using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Queries
{
    public class GetRatesQuery : IQuery<GetRates,List<Rate>>
    {
        private readonly ISessionBuilder<EntityContext> m_SessionBuilder;
        private readonly IListenerCache m_ListenerCache;

        public GetRatesQuery(ISessionBuilder<EntityContext> sessionBuilder, IListenerCache listenerCache)
        {
            m_SessionBuilder = sessionBuilder;
            m_ListenerCache = listenerCache;
        }

        public List<Rate> Ask(GetRates criterion)
        {
            return m_SessionBuilder.Execute(session =>
            {
                var listener = m_ListenerCache.GetByTimeFrame(criterion.Instrument, criterion.TimeFrameType,
                    criterion.TimeFrameValue);

                if (listener == null)
                    return new List<Rate>();

                var ratesDtos = session.Query<RateDto>()
                    .Where(p => p.ListenerId == listener.Id && p.Time >= criterion.StartTime &&
                                p.Time <= criterion.EndTime).OrderBy(p => p.Index).ToList();
                
                if (criterion.OnlyCompleted)
                {
                    ratesDtos = ratesDtos.Where(p => p.Index < listener.LastRateIndex).ToList();
                }

                return ratesDtos.Select(p => p.ToDomain()).ToList();
            });
        }
    }
}
