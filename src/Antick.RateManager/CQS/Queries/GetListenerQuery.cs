﻿using System.Linq;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Queries
{
    public class GetListenerQuery : IQuery<GetListener, ListenerModel>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;

        public GetListenerQuery(ISessionBuilder<EntityContext> session)
        {
            m_Session = session;
        }

        public ListenerModel Ask(GetListener criterion)
        {
            return m_Session.Execute(session =>
            {
                ListenerModel listener = null;

                var listenerDto = session.Query<ListenerDto>()
                    .FirstOrDefault(x => x.Instrument == criterion.Instrument && x.TimeFrameType == criterion.TimeFrame.Type &&
                                x.TimeFrameValue == criterion.TimeFrame.Value);

                listener = listenerDto.ToModel();

                return listener;
            });
        }
    }
}
