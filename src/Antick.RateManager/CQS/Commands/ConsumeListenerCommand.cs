﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Contracts.Apps.RatesApi;
using Antick.Contracts.Domain;
using Antick.Contracts.Msgs.RatesApi;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Misc;
using Antick.Infractructure.Extensions;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.Components.RateAlg;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.Models;
using Castle.Core.Logging;
using EasyNetQ;

namespace Antick.RateManager.CQS.Commands
{
    public class ConsumeListenerCommand: ICommand<ConsumeListenerCommandContext>
    {
        private readonly ILogger m_Logger;
        private readonly IListenerCache m_ListenerCache;
        private readonly IRateAlgFactory m_RateAlgFactory;
        private readonly IBus m_Bus;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IClock m_Clock;

        public ConsumeListenerCommand(ILogger logger, IListenerCache listenerCache, IRateAlgFactory rateAlgFactory, IBus bus, ICommandBuilder commandBuilder, IClock clock)
        {
            m_Logger = logger;
            m_ListenerCache = listenerCache;
            m_RateAlgFactory = rateAlgFactory;
            m_Bus = bus;
            m_CommandBuilder = commandBuilder;
            m_Clock = clock;
        }

        public void Execute(ConsumeListenerCommandContext commandContext)
        {
            var listener = m_ListenerCache.Get(commandContext.ListenerId);

            consume(listener, commandContext.Quotes);

            m_ListenerCache.AddOrUpdate(listener);
        }

        /// <summary>
        /// Основной алгоритм потребеление котировок
        /// </summary>
        /// <param name="listener"></param>
        /// <param name="quote">Новые котировки</param>
        private void consume(ListenerModel listener, List<Quote> quote)
        {
            var quoteFiltered = listener.LastQuoteDate != null
                ? quote.Where(p => p.Time > listener.LastQuoteDate).ToList()
                : quote;
            if (!quoteFiltered.Any())
            {
                m_Logger.DebugFormat("Listener {0} no new quote after filter", listener.TimeFrame);
                return;
            }

            IRateAlg alg = null;
            try
            {
                alg = m_RateAlgFactory.Create(listener.TimeFrame.Type.ToString());

                // Потребление quote этим конкретным RateConsumer
                var instrument = (Instrument)Enum.Parse(typeof(Instrument), listener.Instrument, true);
                var lastQuote = quoteFiltered.Last();

                var algResult = alg.Execute(quoteFiltered, instrument,listener.TimeFrame,listener.LastRate).GetSynchronousResult();

                // обновление данных в БД
                var command = new UpdateListenerCommandContext
                {
                    InsertRates = algResult.InsertRates,
                    UpdatedRates = algResult.UpdatedRates,
                    Id = listener.Id,
                    LastQuoteDate = lastQuote.Time,
                    LastQuoteIndex = lastQuote.Index
                };
                m_CommandBuilder.Execute(command);


                // обновление данных в памяти
                listener.UpdateLastRate(algResult.LastRate);
                listener.UpdateLastQuoteDate(lastQuote.Time);
                listener.UpdateLastQuoteIndex(lastQuote.Index);

                var completedRates = algResult.CompletedRates.Select(map).ToList();
                if (completedRates.Any())
                {
                    var message = new NewRates
                    {
                        CreatedDate = m_Clock.UtcNow(),
                        Rates = completedRates,
                        TimeFrame = listener.TimeFrame,
                        Instrument = new InstrumentComplex
                        {
                            Name = listener.Instrument,
                            Group = InstrumentGroup.Fx,
                            Provider = InstrumentProvider.Oanda
                        }
                    };
                    m_Bus.Publish(message);
                    m_Logger.DebugFormat("Listener {0} Publish NewRates", listener.ToString());
                }
                m_Logger.DebugFormat("Listener {0} consumed. LastQuoteDate {1}",listener.ToString(),listener.LastQuoteDate.ToString());
            }
            finally
            {
                m_RateAlgFactory.Release(alg);
            }
        }

        private Rate map(RateModel model)
        {
            return new Rate
            {
                Time = model.Time,
                Open = model.Open,
                Close = model.Close,
                CloseFact = model.CloseFact,
                High = model.High,
                Low = model.Low,
                Index = model.Index
            };
        }
    }
}
