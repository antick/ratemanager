﻿using System;
using System.Collections.Generic;
using System.Threading;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Misc;
using Antick.Infractructure.Extensions;
using Antick.QuoteManager.Client;
using Antick.RateManager.Components.Locks;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.Models;
using Castle.Core.Internal;
using Castle.Core.Logging;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Потребление активным данным одного конкретного Listener
    /// </summary>
    public class ConsumeListenerFromApiCommand : ICommand<ConsumeListenerFromApiCommandContext>
    {
        private readonly ILogger m_Logger;
        private readonly IListenerCache m_ListenerCache;
        private readonly IQuoteManagerClient m_QuoteManagerClient;
        private readonly IListenerBlocker m_ListenerBlocker;
        private readonly IClock m_Clock;
        private readonly ICommandBuilder m_CommandBuilder;

        public ConsumeListenerFromApiCommand(ILogger logger, IListenerCache listenerCache, IQuoteManagerClient quoteManagerClient, IListenerBlocker listenerBlocker, IClock clock, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_ListenerCache = listenerCache;
            m_QuoteManagerClient = quoteManagerClient;
            m_ListenerBlocker = listenerBlocker;
            m_Clock = clock;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(ConsumeListenerFromApiCommandContext commandContext)
        {
            lock (m_ListenerBlocker.Get(commandContext.ListenerId))
            {
                var listener = m_ListenerCache.Get(commandContext.ListenerId);

                downloadAndConsume(listener);

                if (commandContext.IsStarting)
                {
                    listener.Ready();
                    m_Logger.DebugFormat("Listener {0} is Ready", listener.ToString());
                }
                m_ListenerCache.AddOrUpdate(listener);
            }
        }

        private void downloadAndConsume(ListenerModel listener)
        {
            var day = listener.LastQuoteDate;

            // Если не было данных о последней Quote, то берем из ее первой доступной Quote 
            if (day == null)
            {
                FirstQuoteResponse quoteResponse = null;
                while (true)
                {
                    quoteResponse = m_QuoteManagerClient.GetFirstQuote(listener.Instrument, listener.QuoteTimeFrame).GetSynchronousResult();
                    if (!quoteResponse.IsApiReady)
                    {
                        m_Logger.InfoFormat($"QuoteApi->GetFirstQuote: API is not ready.Try later");
                        Thread.Sleep(10000);
                    }
                    else break;
                }
                day = quoteResponse.Quote.Time;
            }

            while (true)
            {
                try
                {
                    IterationResult iteration = null;

                    iteration = listener.QuoteTimeFrame == "S5"
                        ? iterationByBinaryS5(day, listener)
                        : iterationByAnyTf(day, listener);

                    if (iteration.Day != null)
                        day = iteration.Day;

                    if (iteration.ShouldBreak)
                        break;
                }
                catch (Exception ex)
                {
                    m_Logger.Error(string.Format("Error on consuming alg {0}. Continue after 5 seconds...", listener.ToString()), ex);
                    Thread.Sleep(5000);
                }
            }
        }

        private IterationResult iterationByAnyTf(DateTime? day, ListenerModel listener)
        {
            var startTime = day;
            var endTime = startTime.Value.AddDays(7); // Размер порции 1 неделя

            QuotesResponse quotesResult;

            while (true)
            {
                // Выкачка Quote за указанный день день
                quotesResult = m_QuoteManagerClient
                    .GetQuotesInRange(listener.Instrument,listener.QuoteTimeFrame, startTime.Value, endTime)
                    .GetSynchronousResult();
                if (!quotesResult.IsApiReady)
                {
                    m_Logger.InfoFormat($"QuoteApi->GetQuotes: API is not ready.Try later");
                    Thread.Sleep(10000);
                }
                else break;
            }

            if (quotesResult.Quotes.IsNullOrEmpty())
            {
                m_Logger.InfoFormat($"QuoteApi->GetQuotesInRange: Range from {1} to {2} is empty. Break download", startTime, endTime);
                return new IterationResult { ShouldBreak = true, Day = endTime };
            }
            
            // Кидаем в алгоритм, фильруем, если данные уже были
            consume(listener, quotesResult.Quotes);

            return new IterationResult {Day = endTime};
        }

        private IterationResult iterationByBinaryS5(DateTime? day, ListenerModel listener )
        {
            var isNow = false;

            var now = m_Clock.UtcNow();
            var startTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc);
            var endTIme = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc);
            if (day > startTime && day <= endTIme)
            {
                isNow = true;
            }

            DateTime? lastCompletedDay;
            List<Quote> candles;

            if (!isNow)
            {
                DayResponse quotes = null;
                while (true)
                {
                    // Выкачка Quote за указанный день день
                    quotes = m_QuoteManagerClient.GetQuotes(listener.Instrument, day.Value)
                        .GetSynchronousResult();
                    if (!quotes.IsApiReady)
                    {
                        m_Logger.InfoFormat($"QuoteApi->GetQuotes: API is not ready.Try later");
                        Thread.Sleep(10000);
                    }
                    else break;
                }
                candles = quotes.Candles;
                lastCompletedDay = quotes.LastCompletedDay;
            }
            else
            {
                DayResponse quotes = null;
                while (true)
                {
                    quotes = m_QuoteManagerClient.GetQuotesFrom(listener.Instrument, listener.LastQuoteDate.Value)
                        .GetSynchronousResult();
                    if (!quotes.IsApiReady)
                    {
                        m_Logger.InfoFormat($"QuoteApi->GetQuotesFrom: API is not ready.Try later");
                        Thread.Sleep(10000);
                    }
                    else break;
                }
                candles = quotes.Candles;
                lastCompletedDay = quotes.LastCompletedDay;
            }

            if (!lastCompletedDay.HasValue)
            {
                m_Logger.DebugFormat("Listener [{0}] No complete days with quotes. Break donwloding.", listener,
                    day.Value.ToString("yyyy/MM/dd"));
                return new IterationResult { ShouldBreak = true, Day = day };
            }

            // Обычный день, в которорм есть quotes
            if (!candles.IsNullOrEmpty())
            {
                m_Logger.DebugFormat("Listener [{0}] reading day {1}", listener,
                    day.Value.ToString("yyyy/MM/dd"));
            }
            // в этом дне нет записей
            else
            {
                if (day.Value > lastCompletedDay.Value.AddDays(1))
                {
                    m_Logger.DebugFormat("Listener [{0}] day {1} is future. Break downloading quotes", listener,
                        day.Value.ToString("yyyy/MM/dd"));
                    return new IterationResult { ShouldBreak = true, Day = day };
                }
                else
                {
                    m_Logger.DebugFormat("Listener [{0}] no quote in day {1}", listener,
                        day.Value.ToString("yyyy/MM/dd"));
                    day = day.Value.AddDays(1);
                    return new IterationResult { Day = day };
                }
            }

            // Кидаем в алгоритм, фильруем, если данные уже были
            consume(listener, candles);

            if (isNow)
            {
                m_Logger.DebugFormat("Listener [{0}] day {1} is end (Current day). Break downloading quotes", listener,
                    day.Value.ToString("yyyy/MM/dd"));
                return new IterationResult { ShouldBreak = true, Day = day };
            }

            day = day.Value.AddDays(1);

            return new IterationResult{Day = day};
        }

        /// <summary>
        /// Основной алгоритм потребеление котировок
        /// </summary>
        /// <param name="listener"></param>
        /// <param name="quotes">Новые котировки</param>
        private void consume(ListenerModel listener, List<Quote> quotes)
        {
            m_CommandBuilder.Execute(new ConsumeListenerCommandContext{ListenerId = listener.Id, Quotes = quotes});
        }

        private class IterationResult
        {
            public bool ShouldBreak { get; set; }
            public DateTime? Day { get; set; }
        }

    }
}
