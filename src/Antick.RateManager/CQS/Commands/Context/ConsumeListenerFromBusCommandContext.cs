﻿using System.Collections.Generic;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class ConsumeListenerFromBusCommandContext : ICommandContext
    {
        public int ListenerId { get; set; }

        public List<Quote> Quotes { get; set; }
    }
}
