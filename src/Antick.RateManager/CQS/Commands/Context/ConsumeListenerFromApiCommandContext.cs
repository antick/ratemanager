﻿using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class ConsumeListenerFromApiCommandContext : ICommandContext
    {
        public int ListenerId { get; set; }

        /// <summary>
        /// Первый старт
        /// </summary>
        public bool IsStarting { get; set; }
    }
}
