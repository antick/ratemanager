﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class ConsumeListenersOfInstrumentFromBusCommandContext : ICommandContext
    {
        public string Instrument { get; set; }

        public string TimeFrame { get; set; }

        public List<Quote> Quotes { get; set; }
    }
}
