﻿using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class PrepareAllListenersCommandContext : ICommandContext
    {
        /// <summary>
        /// Если true - значит это первый старт при запуске
        /// </summary>
        public bool IsStarting { get; set; }
    }
}
