﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class ChangeStatusListenerCommandContext : ICommandContext
    {
        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public string Instrument { get; set; }

        public bool Enabled { get; set; }
    }
}
