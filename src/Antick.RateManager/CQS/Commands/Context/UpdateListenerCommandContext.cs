﻿using System;
using System.Collections.Generic;
using Antick.Cqrs.Commands;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Commands.Context
{
    /// <summary>
    /// Команда на обловление всего Listener после обхода в алгоритме IRateAlg
    /// </summary>
    public class UpdateListenerCommandContext : ICommandContext
    {
        /// <summary>
        /// Id Listener
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Обновленные котировки
        /// </summary>
        public List<RateModel> UpdatedRates { get; set; }

        /// <summary>
        /// Массив новых Rate для BulkInsert
        /// </summary>
        public List<RateModel> InsertRates { get; set; }

        /// <summary>
        /// Дата последней используемой Quote
        /// </summary>
        public DateTime LastQuoteDate { get; set; }

        public long LastQuoteIndex { get; set; }

    }
}
