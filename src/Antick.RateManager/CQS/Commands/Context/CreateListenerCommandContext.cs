﻿using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;

namespace Antick.RateManager.CQS.Commands.Context
{
    public class CreateListenerCommandContext : ICommandContext
    {
        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public string Instrument { get; set; }

        public string QuoteTimeFrame { get; set; }

        public bool Enabled { get; set; }
    }
}
