﻿using Antick.Contracts;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.Locks;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Создание нового Listener
    /// </summary>
    public class CreateListenerCommand : ICommand<CreateListenerCommandContext>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly IListenerBlocker m_ListenerBlocker;
        private readonly IListenerCache m_ListenerCache;
        private readonly ICommandBuilder m_CommandBuilder;

        public CreateListenerCommand(ISessionBuilder<EntityContext> session, IQueryBuilder queryBuilder, IListenerBlocker listenerBlocker, IListenerCache listenerCache, ICommandBuilder commandBuilder)
        {
            m_Session = session;
            m_QueryBuilder = queryBuilder;
            m_ListenerBlocker = listenerBlocker;
            m_ListenerCache = listenerCache;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(CreateListenerCommandContext command)
        {
            // Создание в БД записи
            m_Session.Execute(session =>
            {
                var listenerDto = new ListenerDto
                {
                    Instrument = command.Instrument,
                    QuoteTimeFrame = command.QuoteTimeFrame,
                    TimeFrameType = command.TimeFrameType,
                    TimeFrameValue = command.TimeFrameValue,
                    Enabled = command.Enabled
                };
                session.Add(listenerDto);
                session.Save();
            });

            // Вытягиваем полученную доменную модель.
            var listener = m_QueryBuilder.For<ListenerModel>()
                .With(new GetListener
                {
                    Instrument = command.Instrument,
                    TimeFrame = new TimeFrameComplex {Type = command.TimeFrameType, Value = command.TimeFrameValue}
                });

            // Добавляем в список блокировок
            m_ListenerBlocker.Add(listener.Id);

            // Добавляем в кэш Listeners
            m_ListenerCache.AddOrUpdate(listener);

            // Запускаем получение данных для него, если он установлен в сразу в запуск
            if (listener.Enabled)
                m_CommandBuilder.Execute(new ConsumeListenerFromApiCommandContext{ListenerId = listener.Id, IsStarting = true});

        }
    }
}
