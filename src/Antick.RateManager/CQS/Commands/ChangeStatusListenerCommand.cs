﻿using System.Linq;
using Antick.Contracts.Domain;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.Db;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Изменение статуса активности Listener
    /// </summary>
    public class ChangeStatusListenerCommand : ICommand<ChangeStatusListenerCommandContext>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;
        private readonly IListenerCache m_ListenerCache;
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;

        public ChangeStatusListenerCommand(ISessionBuilder<EntityContext> session, IListenerCache listenerCache, IQueryBuilder queryBuilder, ICommandBuilder commandBuilder)
        {
            m_Session = session;
            m_ListenerCache = listenerCache;
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(ChangeStatusListenerCommandContext command)
        {
            var oldStatus = getOldStatus(command);

            // Обновление состояния в Бд
            var id = updateInDb(command);


            // Обновленик статуса в кэше
            var listener = m_ListenerCache.Get(id);
            listener.UpdateEnabled(command.Enabled);
            m_ListenerCache.AddOrUpdate(listener);

            // Включение обратно Listener
            if (oldStatus == false && command.Enabled)
            {
                m_CommandBuilder.Execute(new ConsumeListenerFromApiCommandContext { ListenerId = listener.Id, IsStarting = true });
            }
        }

        private int updateInDb(ChangeStatusListenerCommandContext command)
        {
            return  m_Session.Execute(session =>
            {
                var listenerDto = session.Query<ListenerDto>()
                    .First(p =>
                        p.TimeFrameType == command.TimeFrameType &&
                        p.TimeFrameValue == command.TimeFrameValue &&
                        p.Instrument == command.Instrument);

                listenerDto.Enabled = command.Enabled;

                session.Save();

                return listenerDto.Id;
            });

        }

        private bool getOldStatus(ChangeStatusListenerCommandContext command)
        {
            var listener = m_QueryBuilder.For<ListenerModel>().With(new GetListener
            {
                Instrument = command.Instrument,
                TimeFrame = new TimeFrameComplex { Type = command.TimeFrameType, Value = command.TimeFrameValue }
            });
            return listener.Enabled;
        }
    }
}
