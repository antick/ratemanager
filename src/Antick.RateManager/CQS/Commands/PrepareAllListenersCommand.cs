﻿using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Queries;
using Antick.RateManager.Components.Locks;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Models;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Потребление для всех активных Listeners текущими даннными
    /// </summary>
    public class PrepareAllListenersCommand : ICommand<PrepareAllListenersCommandContext>
    {
        private readonly IQueryBuilder m_QueryBuilder;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly IListenerCache m_ListenerCache;
        private readonly IListenerBlocker m_ListenerBlocker;

        public PrepareAllListenersCommand(IQueryBuilder queryBuilder, ICommandBuilder commandBuilder, IListenerCache listenerCache, IListenerBlocker listenerBlocker)
        {
            m_QueryBuilder = queryBuilder;
            m_CommandBuilder = commandBuilder;
            m_ListenerCache = listenerCache;
            m_ListenerBlocker = listenerBlocker;
        }

        public void Execute(PrepareAllListenersCommandContext commandContext)
        {
            var listeners = m_QueryBuilder.For<List<ListenerModel>>().With<GetActiveListeners>();

            // Построение обьектов для блокировки
            m_ListenerBlocker.Init(listeners.Select(x => x.Id));

            foreach (var listener in listeners)
            {
                m_ListenerCache.AddOrUpdate(listener);

                // Синхронизация этого Listener
                m_CommandBuilder.Execute(
                    new ConsumeListenerFromApiCommandContext
                    {
                        ListenerId = listener.Id,
                        IsStarting = commandContext.IsStarting
                    });
            }
        }
    }
}
