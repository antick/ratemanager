﻿using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Infractructure.Components.Misc;
using Antick.RateManager.Components.Locks;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.Components.RateAlg;
using Antick.RateManager.CQS.Commands.Context;
using Castle.Core.Logging;

namespace Antick.RateManager.CQS.Commands
{
    public class ConsumeListenerFromBusCommand : ICommand<ConsumeListenerFromBusCommandContext>
    {
        private readonly ILogger m_Logger;
        private readonly IListenerCache m_ListenerCache;
        private readonly IRateAlgFactory m_RateAlgFactory;
        private readonly IListenerBlocker m_ListenerBlocker;
        private readonly IClock m_Clock;
        private readonly ICommandBuilder m_CommandBuilder;

        public ConsumeListenerFromBusCommand(ILogger logger, IListenerCache listenerCache,
            IRateAlgFactory rateAlgFactory, IListenerBlocker listenerBlocker, IClock clock, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_ListenerCache = listenerCache;
            m_RateAlgFactory = rateAlgFactory;
            m_ListenerBlocker = listenerBlocker;
            m_Clock = clock;
            m_CommandBuilder = commandBuilder;
        }

        public void Execute(ConsumeListenerFromBusCommandContext commandContext)
        {
            var listener = m_ListenerCache.Get(commandContext.ListenerId);

            lock (m_ListenerBlocker.Get(commandContext.ListenerId))
            {
                // Если новая котировка, это следующая относительно ранее прочитанной - то берем данные как есть
                var isSync = listener.LastQuoteIndex + 1 == commandContext.Quotes.First().Index;
                if (isSync)
                {
                    m_Logger.DebugFormat("Listener {0} is sync. Using quotes from bus", listener.ToString());
                    m_CommandBuilder.Execute(
                        new ConsumeListenerCommandContext
                        {
                            ListenerId = commandContext.ListenerId,
                            Quotes = commandContext.Quotes
                        });
                }

                // иначе - запускаем выкачку через АПИ
                if (!isSync)
                {
                    m_Logger.DebugFormat("Listener {0} is not sync. Starting sync by WebAPi", listener.ToString());
                    m_CommandBuilder.Execute(
                        new ConsumeListenerFromApiCommandContext { ListenerId = commandContext.ListenerId });
                }
            }
        }
    }
}
