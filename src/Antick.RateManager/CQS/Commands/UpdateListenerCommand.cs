﻿using System;
using System.Collections.Generic;
using System.Linq;
using Antick.Cqrs.Commands;
using Antick.Db;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.Db;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Db.Extensions;
using Castle.Core.Logging;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Полное обновление Listener : обновление и загрузка Rates и обновление времени последней активности
    /// </summary>
    public class UpdateListenerCommand : ICommand<UpdateListenerCommandContext>
    {
        private readonly ISessionBuilder<EntityContext> m_Session;
        private readonly ILogger m_Logger;

        public UpdateListenerCommand(ISessionBuilder<EntityContext> session, ILogger logger)
        {
            m_Session = session;
            m_Logger = logger;
        }

        public void Execute(UpdateListenerCommandContext command)
        {
            //Task.Factory.StartNew(() =>
            //{
                m_Session.Execute(session =>
                {
                    if (command.UpdatedRates.Any())
                    {
                        foreach (var updatedRateModel in command.UpdatedRates)
                        {
                            var rateDto = updatedRateModel.ToDbEntity(command.Id);
                            session.Update(rateDto);
                        }
                    }

                    var listener = session.Query<ListenerDto>().First(p => p.Id == command.Id);

                    if (command.InsertRates.Any())
                    {
                        session.BulkInsertAsync(command.InsertRates.Select(x => x.ToDbEntity(command.Id)));
                        listener.LastIndex = command.InsertRates.Last().Index;
                    }
                  
                    listener.LastQuoteDate = command.LastQuoteDate;
                    listener.LastQuoteIndex = command.LastQuoteIndex;

                    session.Save();
                });
            //});
            m_Logger.DebugFormat("Instrument {0} updated downloader status", command.Id);
        }

        public static IEnumerable<List<T>> splitList<T>(List<T> locations, int nSize = 30)
        {
            for (int i = 0; i < locations.Count; i += nSize)
            {
                yield return locations.GetRange(i, Math.Min(nSize, locations.Count - i));
            }
        }
    }
}
