﻿using System.Linq;
using Antick.Cqrs.Commands;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.CQS.Commands.Context;
using Castle.Core.Logging;

namespace Antick.RateManager.CQS.Commands
{
    /// <summary>
    /// Синхронизация всех активных Listeners указанного Инструмента
    /// </summary>
    public class ConsumeListenersOfInstrumentFromBusCommand : ICommand<ConsumeListenersOfInstrumentFromBusCommandContext>
    {
        private readonly IListenerCache m_ListenerCache;
        private readonly ICommandBuilder m_CommandBuilder;
        private readonly ILogger m_Logger;

        public ConsumeListenersOfInstrumentFromBusCommand(IListenerCache listenerCache, ICommandBuilder commandBuilder, ILogger logger)
        {
            m_ListenerCache = listenerCache;
            m_CommandBuilder = commandBuilder;
            m_Logger = logger;
        }

        public void Execute(ConsumeListenersOfInstrumentFromBusCommandContext commandContext)
        {
            // слушатели по этому инструменту
            var listeners = m_ListenerCache.GetByInstrumentAndQuoteTimeFrame(commandContext.Instrument, commandContext.TimeFrame);

            var readyListeners = listeners.Where(p => p.Enabled && p.IsReady).ToList();
            if (!readyListeners.Any())
            {
                m_Logger.DebugFormat("No listeners of {0} is enabled and ready",commandContext.Instrument);
            }

            // Берем только активных слушателей
            foreach (var listener in readyListeners)
            {
                // Синхронизируем этого слушателея
                m_CommandBuilder.Execute(
                    new ConsumeListenerFromBusCommandContext
                    {
                        ListenerId = listener.Id,
                        Quotes = commandContext.Quotes
                    });
            }
        }
    }
}
