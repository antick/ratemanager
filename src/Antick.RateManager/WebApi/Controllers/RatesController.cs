﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Antick.Contracts.Apps.RatesApi;
using Antick.Cqrs.Queries;
using Antick.RateManager.CQS.Queries.Сriteria;
using Antick.RateManager.Models;

namespace Antick.RateManager.WebApi.Controllers
{
    [RoutePrefix("api/rates")]
    public class RatesController : ApiController
    {
        private readonly IQueryBuilder m_QueryBuilder;

        public RatesController(IQueryBuilder queryBuilder)
        {
            m_QueryBuilder = queryBuilder;
        }

        [HttpPost, Route("")]
        public async Task<HttpResponseMessage> GetRates([FromBody] GetRates query)
        {
            var rates = m_QueryBuilder.For<List<Rate>>().With(query);

            var resp = Request.CreateResponse(rates);
            resp.Content.Headers.Add("Content-Logging", "false");
            return await Task.FromResult(resp);
        }

        [HttpPost, Route("getFirst")]
        public async Task<HttpResponseMessage> GetFirstRate([FromBody] GetFirstRate query)
        {
            var rates = m_QueryBuilder.For<Rate>().With(query);

            var resp = Request.CreateResponse(rates);
            resp.Content.Headers.Add("Content-Logging", "false");

            return await Task.FromResult(resp);
        }


        [HttpPost, Route("byLastTime")]
        public async Task<HttpResponseMessage> GetRatesByLastTime([FromBody] GetRatesByLastTime query)
        {
            var rates = m_QueryBuilder.For<List<Rate>>().With(query);

            var resp = Request.CreateResponse(rates);
            resp.Content.Headers.Add("Content-Logging", "false");

            return await Task.FromResult(resp);
        }

        [HttpPost, Route("last")]
        public async Task<HttpResponseMessage> GetRatesLast([FromBody] GetRatesLast query)
        {
            var rates = m_QueryBuilder.For<List<Rate>>().With(query);

            var resp = Request.CreateResponse(rates);
            resp.Content.Headers.Add("Content-Logging", "false");

            return await Task.FromResult(resp);
        }

        [HttpPost, Route("byIndexes")]
        public async Task<HttpResponseMessage> GetRates([FromBody] GetRatesByIndexes query)
        {
            var rates = m_QueryBuilder.For<List<Rate>>().With(query);

            var resp = Request.CreateResponse(rates);
            resp.Content.Headers.Add("Content-Logging", "false");
            return await Task.FromResult(resp);
        }

    }
}
