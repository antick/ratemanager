﻿using System;
using System.Web.Http;
using Antick.ApiHosting.Extensions;
using Inceptum.AppServer;

namespace Antick.RateManager.WebApi.Controllers
{
    public class ConfigController : ApiController
    {
        private static readonly Version m_Version = typeof(ConfigController).Assembly.GetName().Version;
        private readonly Config m_Config;

        public ConfigController(InstanceContext context)
        {
            m_Config = new Config
            {
                Version = m_Version.ToString()
            };

            if (!context.IsProductionMode())
            {
                m_Config.Environment = context.Environment ?? string.Empty;
                m_Config.Node = context.AppServerName ?? string.Empty;
            }
        }

        [HttpGet]
        public Config GetConfig()
        {
            return m_Config;
        }

        [HttpGet]
        public object GetAppStatus()
        {
            return new
            {
                m_Config.Version,
                ApiVersion = Request.GetApiVersion()
            };
        }


        public class Config
        {
            public string Version { get; set; }
            public string Environment { get; set; }
            public string Node { get; set; }

            public Config()
            {
                Environment = "";
                Node = "";
            }
        }

      
    }

    public static class InstanceContextModeDetector
    {
        public static bool IsProductionMode(this InstanceContext context)
        {
            return context != null && Convert.ToString(context.Environment).Equals("PROD", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
