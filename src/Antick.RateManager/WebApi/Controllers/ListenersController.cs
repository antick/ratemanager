﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Antick.Cqrs.Commands;
using Antick.RateManager.CQS.Commands.Context;
using Castle.Core.Internal;
using Inceptum.WebApi.Help.Description;

namespace Antick.RateManager.WebApi.Controllers
{
    [RoutePrefix("api/listeners")]
    public class ListenersController : ApiController
    {
        private readonly ICommandBuilder m_CommandBuilder;

        public ListenersController(ICommandBuilder commandBuilder )
        {
            m_CommandBuilder = commandBuilder;
        }

        [HttpPost, Route("")]
        [ApiExplorerOrder(Order = 4)]
        public async Task<HttpResponseMessage> Add([FromBody] CreateListenerCommandContext command)
        {
            if (command.QuoteTimeFrame.IsNullOrEmpty())
                command.QuoteTimeFrame = "S5";

            m_CommandBuilder.ExecuteAsync(command);
            return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK));
        }

        [HttpPut, Route("")]
        [ApiExplorerOrder(Order = 4)]
        public async Task<HttpResponseMessage> ChangeStatus([FromBody] ChangeStatusListenerCommandContext command)
        {
            m_CommandBuilder.ExecuteAsync(command);
            return await Task.FromResult(Request.CreateResponse(HttpStatusCode.OK));
        }
    }
}
