﻿using Antick.Contracts.Msgs.QuotesApi;
using Antick.Cqrs.Commands;
using Antick.RateManager.CQS.Commands.Context;
using Castle.Core.Logging;
using EasyNetQ.AutoSubscribe;

namespace Antick.RateManager.Components.Consumer
{
    public class NewQuotesConsume : IConsume<NewQuotes>
    {
        private readonly ILogger m_Logger;
        private readonly ICommandBuilder m_CommandBuilder;

        public NewQuotesConsume(ILogger logger, ICommandBuilder commandBuilder)
        {
            m_Logger = logger;
            m_CommandBuilder = commandBuilder;
        }

        public void Consume(NewQuotes message)
        {
            m_Logger.DebugFormat("Received message : {0};", message.Instrument, message.TimeFrame);
            m_CommandBuilder.Execute(
                new ConsumeListenersOfInstrumentFromBusCommandContext
                {
                    Instrument = message.Instrument,
                    TimeFrame = message.TimeFrame,
                    Quotes = message.Quotes
                });
        }
    }
}
