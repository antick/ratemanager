﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Antick.RateManager.Components.Locks
{
    public class ListenerBlocker : IListenerBlocker
    {
        private readonly Dictionary<int, object> m_Blocks;

        public ListenerBlocker()
        {
            m_Blocks = new Dictionary<int, object>();
        }

        public void Init(IEnumerable<int> listernersId)
        {
            foreach (var i in listernersId)
            {
                m_Blocks[i] = new object();
            }
        }

        public void Add(int listenerId)
        {
            m_Blocks[listenerId] = new object();
        }

        public object Get(int listenerId)
        {
            if (!m_Blocks.ContainsKey(listenerId))
                throw new ArgumentException("ListenerId block is not configured");

            return m_Blocks[listenerId];
        }
    }
}
