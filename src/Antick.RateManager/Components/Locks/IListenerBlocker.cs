﻿using System.Collections.Generic;

namespace Antick.RateManager.Components.Locks
{
    public interface IListenerBlocker
    {
        void Init(IEnumerable<int> listernersId);

        void Add(int listenerId);

        object Get(int listenerId);
    }
}
