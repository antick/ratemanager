﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Antick.Contracts;
using Antick.Contracts.Domain;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.MemoryCache
{
    public class ListenerCache : IListenerCache
    {
        private readonly ConcurrentDictionary<int, ListenerModel> m_Listeners;

        public ListenerCache()
        {
            m_Listeners = new ConcurrentDictionary<int, ListenerModel>();
        }

        public void AddOrUpdate(ListenerModel listener)
        {
            m_Listeners.AddOrUpdate(listener.Id, listener, (i, oldListener) => listener);
        }

        public ListenerModel Get(int listenerId)
        {
            return m_Listeners.ContainsKey(listenerId) ? m_Listeners[listenerId] : null;
        }

        public List<ListenerModel> GetByInstrumentAndQuoteTimeFrame(string instrument, string quoteTimeFrame)
        {
            return m_Listeners.Where(x => x.Value.Instrument == instrument && x.Value.QuoteTimeFrame == quoteTimeFrame)
                .Select(x => x.Value).ToList();
        }

        public ListenerModel GetByTimeFrame(string instrument, TimeFrameType timeFrameType, string timeFrameValue)
        {
            var l =  m_Listeners.FirstOrDefault(p => p.Value.Instrument == instrument &&
                                                   p.Value.TimeFrame.Type == timeFrameType &&
                                                   p.Value.TimeFrame.Value == timeFrameValue);
            return l.Key != 0 ? l.Value : null;
        }
    }
}
