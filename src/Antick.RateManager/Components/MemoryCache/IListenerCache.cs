﻿using System.Collections.Generic;
using Antick.Contracts;
using Antick.Contracts.Domain;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.MemoryCache
{
    public interface IListenerCache
    {
        void AddOrUpdate(ListenerModel listener);

        ListenerModel Get(int listenerId);

        List<ListenerModel> GetByInstrumentAndQuoteTimeFrame(string instrument, string quoteTimeFrame);

        ListenerModel GetByTimeFrame(string instrument, TimeFrameType timeFrameType, string timeFrameValue);
    }
}
