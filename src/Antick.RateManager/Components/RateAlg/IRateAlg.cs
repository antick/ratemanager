﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Contracts.Domain;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.RateAlg
{
    public interface IRateAlg
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="quotes">Обрабатываемые первичные данные</param>
        /// <param name="instrument">Инструмент который обрабатываем</param>
        /// <param name="timeFrame">Таймфрейм рабочий, полный данные</param>
        /// <param name="lastRate">Последняя известная котировка, на момент ПЕРЕД запуском алгоритма</param>
        /// <returns></returns>
        Task<AlgResult> Execute(List<Quote> quotes, Instrument instrument, TimeFrameComplex timeFrame, RateModel lastRate);
    }
}
