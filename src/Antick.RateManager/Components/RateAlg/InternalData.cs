﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.RateAlg
{
    public class InternalData
    {
        /// <summary>
        /// новые котировки, на вставку в БД
        /// </summary>
        public List<RateModel> InsertRates { get; set; }

        /// <summary>
        /// Первая котировка
        /// </summary>
        public List<RateModel> UpdatedRates { get; set; }

        /// <summary>
        /// завершенные котировки
        /// </summary>
        public List<RateModel> CompletedRates { get; set; }
    }
}
