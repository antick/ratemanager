﻿using Antick.Contracts.Apps.QuotesApi;
using Antick.Contracts.Domain;
using Antick.Contracts.Specs;
using Antick.Cqrs.Commands;
using Antick.RateManager.CQS.Commands.Context;
using Antick.RateManager.Models;
using Castle.Core.Logging;
using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Antick.RateManager.Components.RateAlg
{
    public class RenkoRateAlg : IRateAlg
    {
        private readonly ILogger m_Logger;

        private bool m_debug = false;

        public RenkoRateAlg(ILogger logger )
        {
            m_Logger = logger;// ?? throw new ArgumentNullException(nameof(logger));
        }

        //private double Step()

        /// <inheritdoc />
        public async Task<AlgResult> Execute(List<Quote> quote, Instrument instrument, TimeFrameComplex timeFrame, RateModel lastRate)
        {
            var last = lastRate;

            var s = Convert.ToInt32(timeFrame.Value);
            var size = s / Math.Pow(10, InstrumentSpec.Get(instrument).Digits);
            var spec = InstrumentSpec.Get(instrument);

            var insertRates = new List<RateModel>();
            var completedRates = new List<RateModel>();
            var updatedRatesDic = new Dictionary<Guid,RateModel>();

            foreach (var candle in quote)
            {
                if (last == null)
                {
                    var rate = new RateModel(Guid.NewGuid(),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        candle.Time, 0);

                    insertRates.Add(rate);
                    completedRates.Add(rate);
                    last = rate;
                   

                    debug("{0} added rate index {1}", timeFrame, rate.Index);
                }
                else
                {
                    var normCandle = normalizeCandle(candle,spec);
                   
                    var internalData = Iterate(last, normCandle, size, spec);

                    foreach (var updatedRate in internalData.UpdatedRates) // запись в общий "буфер" обновленных
                    {
                        updatedRatesDic[updatedRate.Id] = updatedRate;
                    }

                    if (internalData.InsertRates.Any())
                    {
                        last = internalData.InsertRates.Last(); // обновление фактической последней котировки
                        insertRates.AddRange(internalData.InsertRates); // добавление в список котировок для вставки
                        completedRates.AddRange(internalData.CompletedRates); //добавление котировок завершенных (отсылка эвента)
                    }
                }
            }

            // Проверка, обновлять будем только те котировки, которые идут не как вставка(которые ранее уже были)
            List<RateModel> updatedRates = new List<RateModel>();
            var keys = updatedRatesDic.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                if (insertRates.All(p => p.Id != keys[i]))
                {
                    updatedRates.Add(updatedRatesDic[keys[i]]);
                }
            }

            return new AlgResult
            {
                InsertRates = insertRates,
                UpdatedRates = updatedRates,
                CompletedRates = completedRates,
                LastRate = last,
            };
        }
        
        private Quote normalizeCandle(Quote candle, InstrumentSpecItem spec)
        {
            if (candle.Close > candle.Open)
            {
                return new Quote
                {
                    Open = candle.Low,
                    Close = candle.High,
                    High = candle.High,
                    Low = candle.Low,
                    Time = candle.Time
                };
            }
            else if (Math.Abs(candle.Close - candle.Open) < 1 / Math.Pow(10,spec.Digits  + 1))
            {
                return new Quote
                {
                    Open = candle.Close,
                    Close = candle.Close,
                    High = candle.Close,
                    Low = candle.Close,
                    Time = candle.Time
                };
            }
            else
            {
                // движение с Open до High
                return new Quote
                {
                    Open = candle.High,
                    Close = candle.Low,
                    High = candle.High,
                    Low = candle.Low,
                    Time = candle.Time
                };
            }
        }

        private InternalData Iterate(RateModel lastRate, Quote candle, double size, InstrumentSpecItem spec)
        {
            var insertRates  = new List<RateModel>();
            Dictionary<Guid,RateModel> updatedRatesDic = new Dictionary<Guid, RateModel>();
            var completedRates = new List<RateModel>();
            var last = lastRate;

            // новая High точка из хая пред свечи и новой свечи
            var high = new[] { last.High, candle.High }.Max();

            // Новая Low точка из лоя пред свечи и новой свечи
            var low = new[] { last.Low, candle.Low }.Min();

            // Шаг в мажорных пунктах
            var step = Math.Round(Math.Abs(high - low), spec.Digits);
            
            var reamingStep = step;
            while (reamingStep > 0)
            {
                reamingStep -= size;
                
                if (reamingStep >= 0)
                {
                    var closeFact = candle.Close;

                    if (candle.Close > last.Close)
                    {
                        // цена закрытия предыдущей свечи
                        var closePrice = Math.Round(last.Low + size, spec.Digits + 1);
                        
                        last.UpdateClose(closePrice);
                        last.UpdateHigh(closePrice);
                        last.UpdateCloseFact(closeFact);

                        var rate = new RateModel(Guid.NewGuid(), closePrice, closePrice, closeFact, closePrice, closePrice,
                            candle.Time, last.Index + 1);

                        insertRates.Add(rate);
                        updatedRatesDic[last.Id] = last;
                        completedRates.Add(last);

                        last = rate;
                    }
                    else
                    {
                        // цена закрытия предыдущей свечи
                        var closePrice = Math.Round(last.High - size, spec.Digits + 1);

                        last.UpdateClose(closePrice);
                        last.UpdateLow(closePrice);
                        last.UpdateCloseFact(closeFact);

                        var rate = new RateModel(Guid.NewGuid(), closePrice, closePrice, closeFact, closePrice, closePrice,
                            candle.Time, last.Index + 1);

                        insertRates.Add(rate);
                        updatedRatesDic[last.Id] = last;
                        completedRates.Add(last);

                        last = rate;
                    }
                }
                else
                {
                    last.UpdateCloseFact(candle.Close);
                    updatedRatesDic[last.Id] = last;
                }
            }

            return new InternalData
            {
                InsertRates = insertRates,
                UpdatedRates = updatedRatesDic.Select(x => x.Value).ToList(),
                CompletedRates = completedRates
            };
        }

        private void debug(string format, params object[] param)
        {
            if (m_debug)
            {
                m_Logger.DebugFormat(format, param);
            }
        }
    }
}
