﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.RateAlg
{
    public class AlgResult
    {
        /// <summary>
        /// Массив новых Rate для вставки в БД
        /// </summary>
        public List<RateModel> InsertRates { get; set; }

        /// <summary>
        /// Обновленные котировки
        /// </summary>
        public List<RateModel> UpdatedRates { get; set; }

        /// <summary>
        /// Массив новых заверешенных котировок
        /// </summary>
        public List<RateModel> CompletedRates { get; set; }

        /// <summary>
        /// Последняя котировка на момент после обработки алгоритмом
        /// </summary>
        public RateModel LastRate { get; set; }
    }
}
