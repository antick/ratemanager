﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Contracts.Domain;
using Antick.Contracts.Specs;
using Antick.RateManager.Models;

namespace Antick.RateManager.Components.RateAlg
{
    public class TimeRateAlg : IRateAlg
    {


        private TimeSpan GetSpan(TimeFrameComplex timeFrame)
        {
            if (timeFrame.Value[0] == 'M')
            {
                var time = timeFrame.Value.Substring(1, timeFrame.Value.Length - 1);

                if (time.Contains("."))
                    time = time.Replace('.', ',');

                double val = Convert.ToDouble(time);
                var seconds = val * 60;
                return TimeSpan.FromSeconds(seconds);
            }

            throw new Exception("Not implemented");
        }

        public bool IsDayAlign(TimeFrameComplex timeFrame)
        {
            if (timeFrame.Value[0] == 'M')
                return true;

            return false;
        }

        public async Task<AlgResult> Execute(List<Quote> quotes, Instrument instrument, TimeFrameComplex timeFrame,
            RateModel lastRate)
        {
            var last = lastRate;


            var rangeSpan = GetSpan(timeFrame);
            var dayAlign = IsDayAlign(timeFrame);

            var spec = InstrumentSpec.Get(instrument);

            var insertRates = new List<RateModel>();
            var completedRates = new List<RateModel>();
            var updatedRatesDic = new Dictionary<Guid, RateModel>();

            foreach (var candle in quotes)
            {
                if (last == null)
                {
                    var spanInDaySeconds = new TimeSpan(candle.Time.Hour, candle.Time.Minute, candle.Time.Second).TotalSeconds;
                    var rangSpanSeconds = rangeSpan.TotalSeconds;

                    if (spanInDaySeconds % rangSpanSeconds  != 0 )
                        continue;

                    var rate = new RateModel(Guid.NewGuid(),
                        Math.Round(candle.Open, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.Close, spec.Digits - 1),
                        Math.Round(candle.High, spec.Digits - 1),
                        Math.Round(candle.Low, spec.Digits - 1),
                        candle.Time, 0);

                    insertRates.Add(rate);
                    completedRates.Add(rate);
                    last = rate;

                }
                else
                {
                    var internalData = Iterate(last, candle, rangeSpan, dayAlign);

                    foreach (var updatedRate in internalData.UpdatedRates) // запись в общий "буфер" обновленных
                    {
                        updatedRatesDic[updatedRate.Id] = updatedRate;
                    }

                    if (internalData.InsertRates.Any())
                    {
                        last = internalData.InsertRates.Last(); // обновление фактической последней котировки
                        insertRates.AddRange(internalData.InsertRates); // добавление в список котировок для вставки
                        completedRates.AddRange(internalData
                            .CompletedRates); //добавление котировок завершенных (отсылка эвента)
                    }
                }
            }


            // Проверка, обновлять будем только те котировки, которые идут не как вставка(которые ранее уже были)
            List<RateModel> updatedRates = new List<RateModel>();
            var keys = updatedRatesDic.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                if (insertRates.All(p => p.Id != keys[i]))
                {
                    updatedRates.Add(updatedRatesDic[keys[i]]);
                }
            }

            return new AlgResult
            {
                InsertRates = insertRates,
                UpdatedRates = updatedRates,
                CompletedRates = completedRates,
                LastRate = last,
            };
        }

        private InternalData Iterate(RateModel lastRate, Quote candle, TimeSpan rangeSpan,bool dayAlign)
        {
            var insertRates = new List<RateModel>();
            Dictionary<Guid, RateModel> updatedRatesDic = new Dictionary<Guid, RateModel>();
            var completedRates = new List<RateModel>();
            
            // новый бар будет
            if (candle.Time - lastRate.Time >= rangeSpan)
            {
                var lastCandleTime = lastRate.Time;

                while (lastCandleTime + rangeSpan < candle.Time)
                {
                    lastCandleTime += rangeSpan;
                }

                var stubRate = new RateModel(Guid.NewGuid(), lastRate.Close, lastRate.Close, lastRate.Close, lastRate.Close,
                    lastRate.Close,
                    lastCandleTime + rangeSpan, lastRate.Index + 1, candle.Time);

                insertRates.Add(stubRate);

                // 
                completedRates.Add(lastRate);

                lastRate = stubRate;
                lastCandleTime = stubRate.Time;
                
              
            }
            
            var high = new[] { lastRate.High, candle.High }.Max();
            var low = new[] { lastRate.Low, candle.Low }.Min();

            lastRate.UpdateHigh(high);
            lastRate.UpdateLow(low);
            lastRate.UpdateClose(candle.Close);
            lastRate.UpdateCloseFact(candle.Close);

            updatedRatesDic[lastRate.Id] = lastRate;
            

            return new InternalData
            {
                InsertRates = insertRates,
                UpdatedRates = updatedRatesDic.Select(x => x.Value).ToList(),
                CompletedRates = completedRates
            };
        }
    }
}
