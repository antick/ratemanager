﻿namespace Antick.RateManager.Components.RateAlg
{
    public interface IRateAlgFactory
    {
        IRateAlg Create(string componentName);
        void Release(IRateAlg alg);
    }
}
