﻿using System;
using Antick.RateManager.Db.Dto;

namespace Antick.RateManager.Models
{
    public class RateModel
    {
        public Guid Id { get; private set; }

        public double Open { get; private set; }

        public double Close { get; private set; }

        public double CloseFact { get; private set; }

        public double High { get; private set; }

        public double Low { get; private set; }

        public DateTime Time { get; private set; }

        public long Index { get; private set; }

        public DateTime? TimeFact { get; private set; }

        public int ListenerId { get; private set; }

        public RateModel(Guid id, double open, double close, double closeFact, double high, double low, DateTime time, long index, DateTime? timeFact = null)
        {
            Id = id;
            Time = time;
            Open = open;
            Close = close;
            CloseFact = closeFact;
            High = high;
            Low = low;
            Index = index;
            TimeFact = timeFact;
        }

        public void UpdateClose(double close)
        {
            Close = close;
        }

        public void UpdateCloseFact(double closeFact)
        {
            CloseFact = closeFact;
        }

        public void UpdateHigh(double high)
        {
            High = high;
        }

        public void UpdateLow(double low)
        {
            Low = low;
        }

        public void AssignToListener(int listenerId)
        {
            ListenerId = listenerId;
        }
    }
}
