﻿using System;
using Antick.Contracts.Domain;

namespace Antick.RateManager.Models
{
    /// <summary>
    /// Модель RateListener - модель RateListener DbModel
    /// </summary>
    public class ListenerModel
    {
        public int Id { get; private set; }

        /// <summary>
        /// Тайм-фрейм
        /// </summary>
        public TimeFrameComplex TimeFrame { get; private set; }

        /// <summary>
        /// Инструмент
        /// </summary>
        public string Instrument { get; private set; }

        public string QuoteTimeFrame { get; private set; }

        /// <summary>
        /// Включен/выключен
        /// </summary>
        public bool Enabled { get; private set; }

        /// <summary>
        /// Последняя дата используемого quote
        /// </summary>
        public DateTime? LastQuoteDate { get; private set; }

        public long? LastQuoteIndex { get; private set; }
        
        /// <summary>
        /// Влаг готовности, true - когда вычитали при первом старте
        /// </summary>
        public bool IsReady { get; private set; }

        public long? LastRateIndex { get; private set; }

        public RateModel LastRate { get; private set; }
        
        public ListenerModel(int id, string instrument, TimeFrameComplex timeFrame, bool enabled, DateTime? lastQuoteDate, long? lastQuoteIndex, long? lastRateIndex, string quoteTimeFrame)
        {
            Id = id;
            Instrument = instrument;
            TimeFrame = timeFrame;
            Enabled = enabled;
            LastQuoteDate = lastQuoteDate;
            LastQuoteIndex = lastQuoteIndex;
            LastRateIndex = lastRateIndex;
            IsReady = false;
            QuoteTimeFrame = quoteTimeFrame;
        }

        public void Ready()
        {
            IsReady = true;
        }

        public void UpdateLastRate(RateModel rate)
        {
            LastRate = rate;
        }

        public void UpdateLastQuoteDate(DateTime? date)
        {
            LastQuoteDate = date;
        }

        public void UpdateLastQuoteIndex(long? index)
        {
            LastQuoteIndex = index;
        }

        public void UpdateEnabled(bool enabled)
        {
            Enabled = enabled;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Instrument, TimeFrame.ToString());
        }
    }
}
