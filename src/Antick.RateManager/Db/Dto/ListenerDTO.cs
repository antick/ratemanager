using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Antick.Contracts;
using Antick.Contracts.Domain;

namespace Antick.RateManager.Db.Dto
{
    [Table("Listener")]
    public partial class ListenerDto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ListenerDto()
        {
            Rate = new HashSet<RateDto>();
        }

        public int Id { get; set; }

        public TimeFrameType TimeFrameType { get; set; }

        public string TimeFrameValue { get; set; }

        public string Instrument { get; set; }

        public string QuoteTimeFrame { get; set; }

        public bool Enabled { get; set; }

        public DateTime? LastQuoteDate { get; set; }

        public long? LastQuoteIndex { get; set; }

        public long? LastIndex { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RateDto> Rate { get; set; }
    }
}
