using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Antick.RateManager.Db.Dto
{
    [Table("Rate")]
    public partial class RateDto
    {
        public Guid Id { get; set; }

        public int ListenerId { get; set; }

        public double Open { get; set; }

        public double Close { get; set; }

        public double CloseFact { get; set; }

        public double High { get; set; }

        public double Low { get; set; }

        public DateTime Time { get; set; }

        public DateTime? TimeFact { get; set; }

        public long Index { get; set; }

        public virtual ListenerDto Listener { get; set; }
    }
}
