﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts;
using Antick.Contracts.Domain;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Models;

namespace Antick.RateManager.Db.Extensions
{
    public static class ListenerDtoExtensions
    {
        public static ListenerModel ToModel(this ListenerDto dto)
        {
            return new ListenerModel(
                dto.Id, dto.Instrument,
                new TimeFrameComplex {Type = dto.TimeFrameType, Value = dto.TimeFrameValue}, 
                dto.Enabled,
                dto.LastQuoteDate, dto.LastQuoteIndex, dto.LastIndex, dto.QuoteTimeFrame);
        }
    }
}
