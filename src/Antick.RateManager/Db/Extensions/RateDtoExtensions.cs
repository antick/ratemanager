﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Contracts.Apps.RatesApi;
using Antick.RateManager.Db.Dto;
using Antick.RateManager.Models;

namespace Antick.RateManager.Db.Extensions
{
    public static class RateDtoExtensions
    {
        public static RateModel ToModel(this RateDto dto)
        {
            return new RateModel(
                dto.Id,
                dto.Open,
                dto.Close,
                dto.CloseFact,
                dto.High,
                dto.Low,
                dto.Time,
                dto.Index,
                dto.TimeFact);
        }

        public static RateDto ToDbEntity(this RateModel model, int listenerId)
        {
            return new RateDto
            {
                Id = model.Id,
                Time = model.Time,
                Open = model.Open,
                Close = model.Close,
                CloseFact = model.CloseFact,
                High = model.High,
                Low = model.Low,
                Index = model.Index,
                ListenerId = listenerId,
                TimeFact = model.TimeFact
            };
        }

        public static Rate ToDomain(this RateDto dto)
        {
            return new Rate
            {
                Time = dto.Time,
                Open = dto.Open,
                Close = dto.Close,
                CloseFact = dto.CloseFact,
                High = dto.High,
                Low = dto.Low,
                Index = dto.Index,
            };
        }
    }
}
