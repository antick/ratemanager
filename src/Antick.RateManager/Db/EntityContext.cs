using System.Data.Entity;
using Antick.RateManager.Db.Dto;
using Inceptum.AppServer.Configuration;

namespace Antick.RateManager.Db
{
    public partial class EntityContext : DbContext
    {
        public EntityContext(ConnectionString connectionString)
            : base(connectionString)
        {
        }

        public virtual DbSet<RateDto> Rate { get; set; }
        public virtual DbSet<ListenerDto> Listener { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ListenerDto>()
                .HasMany(e => e.Rate)
                .WithRequired(e => e.Listener)
                .WillCascadeOnDelete(false);
        }
    }
}
