﻿using Antick.ApiHosting;
using Antick.ApiHosting.Windsor;
using Antick.Contracts.Apps.QuotesApi;
using Antick.Contracts.Domain;
using Antick.Cqrs;
using Antick.Db;
using Antick.Infractructure.Components.IO;
using Antick.Infractructure.Components.Misc;
using Antick.Messaging.Bus;
using Antick.QuoteManager.Client;
using Antick.RateManager.Components.Locks;
using Antick.RateManager.Components.MemoryCache;
using Antick.RateManager.Components.RateAlg;
using Antick.RateManager.Db;
using Antick.RateManager.WebApi.Configuration;
using Castle.Facilities.Startable;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EasyNetQ.AutoSubscribe;
using Inceptum.AppServer.Configuration;

namespace Antick.RateManager.Windsor
{
    public class BackendInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            installCommonFacilities(container);
            configureApiHosts(container);
            configureRabbit(container);
            configureDb(container);
            configureCqs(container);
            configureComponents(container);
        }

        private void installCommonFacilities(IWindsorContainer container)
        {
            // Common facilities
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.AddFacility<StartableFacility>(f => f.DeferredStart());
            container.AddFacility<TypedFactoryFacility>();
            //container.AddFacility<QuartzFacility>();

            // Config
            container.AddFacility<ConfigurationFacility>(f => f.ConfigureConnectionStrings("transports", "{environment}", "{machineName}"));
            
            container.Register(Component.For<LoggingInterceptor>());
        }

        private void configureRabbit(IWindsorContainer container)
        {
            // настройка подключения
            container.Install(new BusInstaller("transports", "EasyNetQ"));

            // регистрация всех IConsume реализаций
            container.Register(Classes.FromThisAssembly().BasedOn(typeof(IConsume<>)).WithServiceAllInterfaces().WithServiceSelf());

            // Автоподписка на сообщения типа IConsume
            container.Register(Component.For<AutoSubscriber>()
                .UsingFactoryMethod(kernel =>  AutoSubscriberFactory.BasicAutoDelete(container, "RateManager"))
                .LifestyleSingleton());
        }
        
        private void configureApiHosts(IWindsorContainer container)
        {
            container.Install(new DefaultApiHostingInstaller<MainApiHostConfigurator>(
                Types.FromAssemblyInThisApplication(), "infractructure", "ApiHost", "Network"));
        }

        /// <summary>
        /// Конфигурация Db и Db сессий
        /// </summary>
        /// <param name="container"></param>
        private void configureDb(IWindsorContainer container)
        {
            container.Install(new SessionBuilderWindsorInstaller<EntityContext>("main"));
        }

        
        /// <summary>
        /// Конфигурация процессора команд и запросов
        /// </summary>
        /// <param name="container"></param>
        private void configureCqs(IWindsorContainer container)
        {
            container.Install(new CqrsInstaller(Types.FromAssemblyInThisApplication()));
        }

        private void configureComponents(IWindsorContainer container)
        {
            container.Register(Component.For<IListPackage<Quote>>().ImplementedBy<ListPackage<Quote>>());
            container.Register(Component.For<IQuoteManagerClient>()
                .ImplementedBy<QuoteManagerClient>()
                .DependsOnBundle("dependencies", "QuoteManagerClient", "{environment}", "{machineName}"));

            container.Register(Component.For<IRateAlgFactory>()
                .AsFactory(c => c.SelectedWith(new NamedTypedFactoryComponentSelector())));
            container.Register(Component.For<IRateAlg>().ImplementedBy<RenkoRateAlg>().Named(TimeFrameType.Renko.ToString()));
            container.Register(Component.For<IRateAlg>().ImplementedBy<TimeRateAlg>().Named(TimeFrameType.Time.ToString()));

            container.Register(Component.For<IListenerCache>().ImplementedBy<ListenerCache>().LifestyleSingleton());
            container.Register(Component.For<IListenerBlocker>().ImplementedBy<ListenerBlocker>().LifestyleSingleton());

            container.Register(Component.For<IClock>().ImplementedBy<Clock>().LifestyleSingleton());
        }
    }
}
