﻿using System.Reflection;
using Castle.Facilities.TypedFactory;

namespace Antick.RateManager.Windsor
{
    public class NamedTypedFactoryComponentSelector : DefaultTypedFactoryComponentSelector
    {
        protected override string GetComponentName(MethodInfo method, object[] arguments)
        {
            if (method.Name == "Create" && arguments.Length > 0 && arguments[0] is string)
            {
                return (string)arguments[0];
            }
            return base.GetComponentName(method, arguments);
        }
    }
}
